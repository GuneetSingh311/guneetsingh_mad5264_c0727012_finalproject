//
//  ViewController.swift
//  imageTracking
//
//  Created by Guneet on 2019-04-07.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import ReplayKit
import AVKit
import AVFoundation

class ViewController: UIViewController, ARSCNViewDelegate,SCNPhysicsContactDelegate {

    //let music1 = Bundle.main.url(forResource: "penapple1", withExtension: ".mp3")
    let music2 = Bundle.main.url(forResource: "penapple2", withExtension: ".mp3")
    var avplayer = AVPlayer()
   
    var coatValue = false
    var hatValue = false
    
    @IBOutlet weak var ppaptextARView: ARSKView!
    @IBOutlet var sceneView: ARSCNView!
    var videNode = SKSpriteNode()
    var videNode2 = SKSpriteNode()
    var videNode3 = SKSpriteNode()
    let videoScene1 = SKScene(size: CGSize(width:720 , height:1280))
    let videoScene2 = SKScene(size: CGSize(width:720 , height:1280))
    
    @IBOutlet weak var coatARView: ARSKView!
    @IBOutlet weak var hatARView: ARSKView!
    
    var shapeNode = [SCNNode]()
    var musicisPlaying = false
    var dragging = true
    // Firebase variables
    let recorder = RPScreenRecorder.shared()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.musicisPlaying = false
        self.avplayer = AVPlayer(url: music2!)
       
        if(musicisPlaying == true) {
           self.avplayer.pause()
        }
        coatARView.isHidden = true
        hatARView.isHidden = true
        ppaptextARView.isHidden = true
        // Set the view's delegate
        sceneView.delegate = self
        // Show statistics such as fps and timing information
       // sceneView.showsStatistics = true
        sceneView.scene.physicsWorld.contactDelegate = self
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        sceneView.scene = scene
        
        // scene configuration
        let configuration = ARImageTrackingConfiguration()
        guard let resource = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)
            else {
                print("no image")
                return
}
        
        // configuration.trackingImages = resource
        configuration.trackingImages = resource
        configuration.maximumNumberOfTrackedImages = 2
        
        // Run the view's session
        //sceneView.session.run(configuration)
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
}
    
    
@IBAction func putCoatonScreen(_ sender: Any) {
       if(coatValue==false)
       {
        self.coatARView.isHidden = true
        coatValue = true
        }
        else if(coatValue == true)
       {
        self.coatARView.isHidden = false
        coatValue = false
        }
}
    
    
    @IBAction func putHatOnScreen(_ sender: Any) {
        if(hatValue==false)
        {
            self.hatARView.isHidden = true
            hatValue = true
        }
        else if(hatValue == true)
        {
            self.hatARView.isHidden = false
            hatValue = false
        }
    }
    
    
    
    
    
    
@IBAction func stopRecording(_ sender: Any) {
     avplayer.pause()
        recorder.stopRecording {
            (sceneView, error) in
            if let previewVC  = sceneView {
               
                previewVC.previewControllerDelegate = self
                
                self.present(previewVC, animated: true,completion: nil)
                    self.avplayer.pause()
            
}
            if let error = error {
                print(error)
            }
        }
}
    
    
@IBAction func recordAction(_ sender: Any) {
recorder.startRecording { (error) in
            if let error = error {
            print(error)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.musicisPlaying = false
        self.avplayer = AVPlayer(url: music2!)
        if(musicisPlaying == true) {
            self.avplayer.pause()
        }
        coatARView.isHidden = true
      let configuration = ARImageTrackingConfiguration()
        guard let resource = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)
            else {
                print("no image")
                return
}
        // configuration.trackingImages = resource
        configuration.trackingImages = resource
        configuration.maximumNumberOfTrackedImages = 2
        // Run the view's session
        //sceneView.session.run(configuration)
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
}
    func resetTracking() {
        self.avplayer.pause()
        let configuration = ARImageTrackingConfiguration()
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
}
    
    // MARK: - ARSCNViewDelegate
    // adding node to the screen
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     
        let node = SCNNode()
        
        // for apple
        videNode = SKSpriteNode(imageNamed: "apple1.png")
        videNode.position = CGPoint(x: videoScene1.size.width/2, y: videoScene1.size.height/2)
        videNode.size = videoScene1.size
        videNode.yScale = -1
        videoScene1.addChild(videNode)
        
        // for pen
        videNode2 = SKSpriteNode(imageNamed: "fountain-pen.png")
        videNode2.position = CGPoint(x: videoScene2.size.width/2, y: videoScene2.size.height/2)
        videNode2.size = videoScene2.size
        videNode2.yScale = -1
        videoScene2.addChild(videNode2)
        
     
        
        
        // ARIMAGEANCHOR: OBJECT pointing to images in source.
        if let imageAnchor = anchor as? ARImageAnchor {
        let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
            
            var planeNode = SCNNode(geometry: plane)
            if imageAnchor.referenceImage.name == "apple" {
            plane.firstMaterial?.diffuse.contents = videoScene1
            planeNode = SCNNode(geometry: plane)
            }

            else if(imageAnchor.referenceImage.name == "pen")
            {
           
            plane.firstMaterial?.diffuse.contents = videoScene2
            planeNode = SCNNode(geometry: plane)
}
           

            
            // Rotate the plane to match the anchor
            planeNode.eulerAngles.x = -.pi / 2
            // Add plane node to parent
             node.addChildNode(planeNode)
             self.shapeNode.append(node)
            }
            return node
}
   
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
      
        if shapeNode.count == 2 {
        // create vector coordinates from node to get its position
        let vector1 = SCNVector3ToGLKVector3(self.shapeNode[0].position)
        let vector2 = SCNVector3ToGLKVector3(self.shapeNode[1].position)
        let distance1 = GLKVector3Distance(vector1, vector2)
            print(distance1)
            if distance1 < 0.05 {
                avplayer.play()
                self.musicisPlaying = true
                print("Collision")
                 DispatchQueue.main.async {
                    self.ppaptextARView.isHidden = false
}
            }
            
            else {
                DispatchQueue.main.async {
                    self.ppaptextARView.isHidden = true
                    self.avplayer.pause()
                }
          
            }
        }
}
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let touchPoint = touch?.location(in: self.coatARView)
        let touchPoint2 = touch?.location(in: self.hatARView)
        
        if (dragging) {
            
            coatARView.center = CGPoint(x: (touchPoint?.x)!, y: (touchPoint?.y)!)
            hatARView.center =  CGPoint(x: (touchPoint2?.x)!, y: (touchPoint2?.y)!)
        }
        
    }
        
        
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
extension ViewController: RPPreviewViewControllerDelegate{
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true, completion: nil)
    }
}

