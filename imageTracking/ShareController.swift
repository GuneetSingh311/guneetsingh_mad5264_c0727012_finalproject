//
//  ShareController.swift
//  imageTracking
//
//  Created by Guneet on 2019-04-17.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import AVKit
import AVFoundation

class ShareController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    var ref: DatabaseReference!
    @IBOutlet weak var videoTable: UITableView!
    var data = [DataFromServer]()
    var values = [AnyObject]()
    override func viewDidLoad() {
    ref = Database.database().reference(fromURL: "https://applepen-85d51.firebaseio.com/")
        super.viewDidLoad()
        videoTable.delegate = self
        videoTable.dataSource = self
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (Timer) in
            self.videoTable.reloadData()
        }
       
       ref.observe(DataEventType.value, with: { (snapshot) in
            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
        if (postDict == nil) {
            print("No values")
        }
        //print(postDict["URLS"]!)
        for (k,v) in postDict {
        self.data.append(DataFromServer(Key: k, Value: v))
        print(k)
        print(v)
        }
})

       

}

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
}
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VideoFromDatabaseCell
    cell.textLabel?.text = "VideoURL: \(data[indexPath.row].values as! String)"
        return cell
}
    
    
    var index = 0
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       self.index = indexPath.row
        performSegue(withIdentifier: "toVideo", sender: nil)
}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "toVideo"){
        let vc = segue.destination as! VideoController
        vc.videoURL = data[index].values as? String
        }
        }
    
}
