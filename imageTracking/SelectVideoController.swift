//
//  SelectVideoController.swift
//  imageTracking
//
//  Created by Guneet on 2019-04-17.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class SelectVideoController: UIViewController{

    let pickVideo = UIImagePickerController()
    @IBOutlet weak var showVideoPreview: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    var  videoURL:NSURL?
    
    @IBOutlet weak var fileName: UITextField!
    @IBOutlet weak var selectVideo: UIButton!
    
    @IBOutlet weak var progress: UILabel!
    @IBAction func selectVideoAction(_ sender: Any) {
        pickVideo.sourceType = .savedPhotosAlbum
        pickVideo.delegate = self
        pickVideo.mediaTypes = ["public.image", "public.movie"]
        present(pickVideo, animated: true, completion:nil)
}
    
    
@IBAction func shareButtonAction(_ sender: Any) {

    // File located on disk
    let localFile = self.videoURL
    // Create a reference to the file you want to upload
    let storageRef = Storage.storage().reference().child("\(fileName.text!)")
    let firebaseStorage = Database.database().reference().childByAutoId()
    
        // Upload the file to the path in storage
    if localFile == nil {
        print("NO item selected")
    }
    else {
        let uploadTask = storageRef.putFile(from: localFile as! URL, metadata: nil) { metadata, error in
        guard let metadata = metadata else {
            // Uh-oh, an error occurred!
            print(error)
            return
            }
            
        // show progressLabel
        self.progress.isHidden = false
        // Metadata contains file metadata such as size, content-type.
        storageRef.downloadURL { (url, error) in
            guard let downloadURL = url else {
                // Uh-oh, an error occurred!
               print("nourl")
                return
}
            // add url of storage to firebase database.
            firebaseStorage.setValue(downloadURL.absoluteString)
            print(downloadURL)
        }
    }
    }
}
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progress.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            view.addGestureRecognizer(tap)
}
    // hide keyboard on touch.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
}
}

// using imagepicker to select video and its url from gallery.
extension SelectVideoController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        videoURL = info[.mediaURL] as? NSURL
        print("videoURL:\(String(describing: videoURL))")
        self.dismiss(animated: true, completion: nil)
}
}


