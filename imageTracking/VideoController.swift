//
//  VideoController.swift
//  imageTracking
//
//  Created by Guneet on 2019-04-21.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import  AVKit
import AVFoundation
import WebKit
class VideoController: UIViewController {

    @IBOutlet weak var videoview: WKWebView!
    var videoURL:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: URL(string: videoURL!)!)
        self.videoview.load(request)
}
}
